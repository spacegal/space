#!/usr/bin/env sh

#
# Check for Lua libraries
#
set -o nounset

if command -v "lua5.1" >/dev/null 2>&1 ; then
    for _lib_name in "lyaml" "base64" "cjson" "posix"; do
        if ! "lua5.1" -e "require '${_lib_name}'" 2>/dev/null ; then
            printf "FAIL: could not find library %s\n" "$_lib_name" 1>&2
            exit 1
        else
            printf "OK: %s\n" "$_lib_name"
        fi
    done
else
    printf "FAIL: missing lua5.1 program\n" 1>&2
    exit 1
fi

