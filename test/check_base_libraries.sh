#!/usr/bin/env sh

#
# Check for base system libraries
#
set -o nounset

_check_library()
{
    local _lib_name=$1
    local _output=

    for _ext in ".a" ".so" ".so.*"; do
        _output=$(find /usr/lib -name "${_lib_name}${_ext}" -print)
        if [ -n "$_output" ]; then
            return 0
        fi

        _output=$(find /usr/local/lib -name "${_lib_name}${_ext}" -print)
        if [ -n "$_output" ]; then
            return 0
        fi
    done

    printf "FAIL: could not find library %s\n" "$_lib_name" 1>&2
    exit 1
}

_YAML_LIB_NAME="libyaml"
_check_library "$_YAML_LIB_NAME"
printf "OK: %s\n" "$_YAML_LIB_NAME"

